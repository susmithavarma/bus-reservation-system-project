package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class Agent_Login_Page extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	Connection conn = Jdbc_Connection.conn();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Agent_Login_Page frame = new Agent_Login_Page();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 */
	public Agent_Login_Page() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Agent Login Page");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("UserName:");
		lblUsername.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblUsername.setBounds(100, 91, 82, 14);
		contentPane.add(lblUsername);
		
		textField = new JTextField();
		textField.setBounds(183, 88, 137, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblPassword.setBounds(100, 141, 82, 14);
		contentPane.add(lblPassword);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String value1=textField.getText(); 
				String value2=passwordField.getText(); 

				try{ 
				Statement st = conn.createStatement(); 
				System.out.println("hi");
				String qry = "select UserName,Password from agent_details where username='"+value1+"' and password='"+value2+"'" ;
				System.out.println(qry);
				ResultSet res = st.executeQuery(qry); 
				System.out.println("h");
				System.out.println(value1);
				System.out.println(value2);
			//	System.out.println(res.getString("USERNAME"));
				if(textField.getText().length() == 0 || passwordField.getText().length()== 0)      //check condition null
				{
				JOptionPane.showMessageDialog(null,"Please enter valid username and password");
				}
				else if(res.next()==true) { 
					JOptionPane.showMessageDialog(null,"login successfull");	
			Ticket_Details1 td = new Ticket_Details1();
			td.setVisible(true);
			dispose();
				} 
				else{ 
				//	System.out.println(res.getString("USERNAME"));
				JOptionPane.showMessageDialog(null,"Incorrect login or password"); 
				}
			}
				catch(Exception e1){ 
				System.out.println(e1.getMessage()); 
				}
			}
		});
		btnLogin.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnLogin.setBounds(173, 201, 89, 23);
		contentPane.add(btnLogin);
		
		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Login_Page lp = new Login_Page();
			lp.setVisible(true);
			dispose();
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(308, 201, 89, 23);
		contentPane.add(btnCancel);
		
		ImageIcon image = new ImageIcon("E:\\Vengared.PNG");
		JLabel lblNewLabel = new JLabel(image);
		lblNewLabel.setBounds(159, 11, 101, 51);
		contentPane.add(lblNewLabel);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Agent_Registration ar = new Agent_Registration();
			ar.setVisible(true);
			dispose();
			}
		});
		btnRegister.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnRegister.setBounds(37, 201, 89, 23);
		contentPane.add(btnRegister);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(183, 138, 137, 20);
		contentPane.add(passwordField);
	}
}
