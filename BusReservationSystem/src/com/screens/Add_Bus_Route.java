package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JTextField;
import javax.swing.JButton;

public class Add_Bus_Route extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	Connection conn = Jdbc_Connection.conn();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Add_Bus_Route frame = new Add_Bus_Route();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 */
	public Add_Bus_Route() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle(" Adding BusRoute");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRouteId = new JLabel("Route Id:");
		lblRouteId.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblRouteId.setBounds(64, 43, 85, 14);
		contentPane.add(lblRouteId);
		
		JLabel lblSource = new JLabel("Source:");
		lblSource.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblSource.setBounds(64, 88, 61, 14);
		contentPane.add(lblSource);
		
		JLabel lblDestination = new JLabel("Destination:");
		lblDestination.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblDestination.setBounds(64, 131, 85, 14);
		contentPane.add(lblDestination);
		
		textField = new JTextField();
		textField.setBounds(159, 40, 123, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(159, 85, 123, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(159, 128, 123, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String route_id=textField.getText();
				String source=textField_1.getText();
				String destination=textField_2.getText();
			try{ 
		
			String qry = "Insert into bus_route(route_id,source,destination)values(?,?,?)";
			PreparedStatement st = conn.prepareStatement(qry);
			System.out.println(qry);
	        
			if(textField.getText().length() == 0 || textField_1.getText().length()== 0|| textField_2.getText().length()== 0)      //check condition null
			{
			JOptionPane.showMessageDialog(null,"Please enter valid details");
			}
			else
			{
				st.setString(1, textField.getText());
				st.setString(2, textField_1.getText());
				st.setString(3, textField_2.getText());
				st.executeUpdate();	
			JOptionPane.showMessageDialog(null, "added Successfully");
			
			conn.close();
			//dispose();
			}
			}
			catch(Exception e1){ 
				System.out.println(e1.getMessage()); 
				} 
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
			}	
		
		});
		
		
		btnAdd.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnAdd.setBounds(211, 191, 89, 23);
		contentPane.add(btnAdd);
		
		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Bus_Details bd = new Bus_Details();
			bd.setVisible(true);
			dispose();
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(335, 191, 89, 23);
		contentPane.add(btnCancel);
	}
}
