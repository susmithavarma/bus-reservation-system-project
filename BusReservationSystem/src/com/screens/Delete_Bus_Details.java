package com.screens;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class Delete_Bus_Details extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	JComboBox<String> comboBox = new JComboBox();
	Connection conn = Jdbc_Connection.conn();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Delete_Bus_Details frame = new Delete_Bus_Details();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public Delete_Bus_Details() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle(" Delete BusDetails");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBusId = new JLabel("Bus Id:");
		lblBusId.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblBusId.setBounds(72, 83, 76, 14);
		contentPane.add(lblBusId);
		
		
		JButton btnDelete = new JButton("Delete");
		
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String getItem = comboBox.getSelectedItem().toString().trim();
			try{ 
				String query ="delete from bus_details where bus_id=?";
				PreparedStatement pstmt = conn.prepareStatement(query);
				if (getItem.equals("SelectBusId")) {
					JOptionPane.showMessageDialog(null,"Please select the BusID");
				} else {
					
						pstmt.setString(1, getItem);
						pstmt.executeUpdate();
						JOptionPane.showMessageDialog(null, "deleted Successfully");
						comboBox.removeAllItems();
						query ="select bus_id from bus_details";
						Statement st = conn.createStatement();
						ResultSet rset=st.executeQuery(query);
						comboBox.addItem("SelectBusId");
						while(rset.next())
					    {
							comboBox.addItem(rset.getString(1));
					    }
					} 
				}
			
			catch(Exception e1){ 
				System.out.println(e1.getMessage()); 
				} 
			
			comboBox.setSelectedItem("SelectBusId");
			}	
		
		});
		
		
		btnDelete.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnDelete.setBounds(137, 172, 89, 23);
		contentPane.add(btnDelete);
		
		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Bus_Details bd = new Bus_Details();
			bd.setVisible(true);
			dispose();
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(252, 172, 89, 23);
		contentPane.add(btnCancel);
		
		
		String query ="select bus_id from bus_details";
		Statement st = conn.createStatement();
		ResultSet rset=st.executeQuery(query);
		comboBox.addItem("SelectBusId");
		while(rset.next())
	    {
			comboBox.addItem(rset.getString(1));
	    }
		comboBox.setBounds(137, 81, 121, 20);
		contentPane.add(comboBox);
	}
}
