package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class Delete_Bus_Route extends JFrame {

	private JPanel contentPane;
	Connection conn = Jdbc_Connection.conn();
	JComboBox RoutecomboBox = new JComboBox();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Delete_Bus_Route frame = new Delete_Bus_Route();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public Delete_Bus_Route() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle(" Delete BusRoute");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRouteId = new JLabel("Route Id:");
		lblRouteId.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblRouteId.setBounds(79, 82, 78, 14);
		contentPane.add(lblRouteId);
		
		JButton btnDelete = new JButton("Delete");
		
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String route_id=RoutecomboBox.getSelectedItem().toString().trim();
				try {  
					String qry ="delete from bus_route where route_id=?";
					PreparedStatement st = conn.prepareStatement(qry);
					if(route_id.equals("SelectRouteId"))      //check condition null
					{
						JOptionPane.showMessageDialog(null,"Please enter valid details ");
					}
					else {
						st.setString(1, route_id);
						st.executeUpdate();
						JOptionPane.showMessageDialog(null, "deleted Successfully");
						conn.close();
						//dispose();
					}
				} catch(Exception e1){ 
					System.out.println(e1.getMessage()); 
				} 
				RoutecomboBox.setSelectedItem("SelectRouteId");
				
			}	
		});
		
		
		btnDelete.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnDelete.setBounds(205, 163, 89, 23);
		contentPane.add(btnDelete);
		
		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Bus_Details bd = new Bus_Details();
			bd.setVisible(true);
			dispose();
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(335, 163, 89, 23);
		contentPane.add(btnCancel);
		
		
		String query ="select route_id  from bus_route";
		Statement st = conn.createStatement();
		ResultSet rset=st.executeQuery(query);
		RoutecomboBox.addItem("SelectRouteId");
		while(rset.next())
	    {
			RoutecomboBox.addItem(rset.getString(1));
	    }
		RoutecomboBox.setBounds(112, 143, 132, 20);
		contentPane.add(RoutecomboBox);
		RoutecomboBox.setBounds(159, 80, 112, 20);
		contentPane.add(RoutecomboBox);
	}

}
