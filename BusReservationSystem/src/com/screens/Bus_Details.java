package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.awt.Color;
import javax.swing.JButton;

public class Bus_Details extends JFrame {

	private JPanel contentPane;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bus_Details frame = new Bus_Details();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 */
	public Bus_Details() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("BusDetails Page");
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnBusdetails = new JMenu("BusDetails");
		mnBusdetails.setForeground(new Color(0, 0, 0));
		mnBusdetails.setBackground(new Color(0, 102, 255));
		mnBusdetails.setFont(new Font("Times New Roman", Font.BOLD, 12));
		menuBar.add(mnBusdetails);
		
		JMenuItem mntmAdd = new JMenuItem("Add");
		mntmAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Add_Bus_Details ab;
			try {
				ab = new Add_Bus_Details();
				ab.setVisible(true);
				dispose();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			}
		});
		mnBusdetails.add(mntmAdd);
		
		JMenuItem mntmDelete = new JMenuItem("Delete");
		mntmDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Delete_Bus_Details db = null;
			try {
				db = new Delete_Bus_Details();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			db.setVisible(true);
			dispose();
			}
		});
		mnBusdetails.add(mntmDelete);
		
		JMenuItem mntmView = new JMenuItem("View");
		mntmView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Bus_Details_Table bd = new Bus_Details_Table();
			//bd.setVisible(true);
			//dispose();
			}
		});
		mnBusdetails.add(mntmView);
		
		JMenu mnBusroute = new JMenu("BusRoute");
		mnBusroute.setForeground(new Color(0, 0, 0));
		mnBusroute.setFont(new Font("Times New Roman", Font.BOLD, 12));
		menuBar.add(mnBusroute);
		
		JMenuItem mntmAdd_1 = new JMenuItem("Add");
		mntmAdd_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Add_Bus_Route abr = new Add_Bus_Route();
			abr.setVisible(true);
			dispose();
			}
		});
		mnBusroute.add(mntmAdd_1);
		
		JMenuItem mntmDelete_1 = new JMenuItem("Delete");
		mntmDelete_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Delete_Bus_Route dbr;
			try {
				dbr = new Delete_Bus_Route();
				dbr.setVisible(true);
				dispose();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			}
		});
		mnBusroute.add(mntmDelete_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuItem mntmView_1 = new JMenuItem("View");
		mntmView_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Bus_Route_Table bd = new Bus_Route_Table();
			//bd.setVisible(true);
			//dispose();
			}
		});
		mnBusroute.add(mntmView_1);
		
		
		JButton btnBack = new JButton("LogOut");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Admin_Login_Page alp = new Admin_Login_Page();
			alp.setVisible(true);
			dispose();
			}
		});
		btnBack.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnBack.setBounds(301, 179, 89, 23);
		contentPane.add(btnBack);
	}
}
