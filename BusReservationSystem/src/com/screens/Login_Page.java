package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

public class Login_Page extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login_Page frame = new Login_Page();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 */
	public Login_Page() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Login Page");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		ImageIcon image = new ImageIcon("E:\\Vengared.PNG");
		JLabel lblNewLabel = new JLabel(image);
		lblNewLabel.setBounds(157, 31, 101, 51);
		contentPane.add(lblNewLabel);
		
		JButton btnAdminLogin = new JButton("Admin Login");
		btnAdminLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Admin_Login_Page alp = new Admin_Login_Page();
			alp.setVisible(true);
			dispose();
			}
		});
		
		btnAdminLogin.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnAdminLogin.setBounds(162, 110, 118, 23);
		contentPane.add(btnAdminLogin);
		
		JButton btnAgentLogin = new JButton("Agent Login");
		btnAgentLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Agent_Login_Page al = new Agent_Login_Page();
			al.setVisible(true);
			dispose();
			}
		});
		btnAgentLogin.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnAgentLogin.setBounds(162, 160, 118, 23);
		contentPane.add(btnAgentLogin);
		
		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Welcome_page wp = new Welcome_page();
			wp.setVisible(true);
			dispose();
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(335, 227, 89, 23);
		contentPane.add(btnCancel);
	}
}
