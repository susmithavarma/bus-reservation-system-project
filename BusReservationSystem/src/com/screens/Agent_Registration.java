package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class Agent_Registration extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private JPasswordField passwordField;
	Connection conn = Jdbc_Connection.conn();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Agent_Registration frame = new Agent_Registration();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}
	
	/**
	 * Create the frame.
	 */
	public static boolean isNumber(String str)
	{

		 try  
		  {  
		    double d = Double.parseDouble(str);  
		  }  
		  catch(NumberFormatException nfe)  
		  {  
		    return false;  
		  }  
		  return true;  
	}
	
	
	public Agent_Registration() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Agent Registration Page");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("UserName:");
		lblUsername.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblUsername.setBounds(56, 29, 83, 14);
		contentPane.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblPassword.setBounds(56, 75, 70, 14);
		contentPane.add(lblPassword);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblEmail.setBounds(56, 122, 70, 14);
		contentPane.add(lblEmail);
		
		ImageIcon image = new ImageIcon("E:\\Vengared.PNG");
		JLabel lblNewLabel = new JLabel(image);
		lblNewLabel.setBounds(333, 0, 101, 51);
		contentPane.add(lblNewLabel);
		
		JLabel lblPhonenumber = new JLabel("PhoneNumber:");
		lblPhonenumber.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblPhonenumber.setBounds(56, 166, 83, 14);
		contentPane.add(lblPhonenumber);
		
		textField = new JTextField();
		textField.setBounds(171, 26, 136, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(171, 116, 136, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(171, 163, 136, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnRegister = new JButton("Register");
		
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					EmailValidator emailValidator = new EmailValidator();
					String username1=textField.getText();
					String password1=passwordField.getText();
					String email=textField_2.getText();
					String phNumber=textField_3.getText();
					if(textField.getText().length() == 0 || passwordField.getText().length()== 0||textField_2.getText().length() == 0||textField_3.getText().length() == 0)      //check condition null
					{
					JOptionPane.showMessageDialog(null,"Please enter valid details");
					}
					else if(textField_3.getText().length()<10||textField_3.getText().length()>10) {
						JOptionPane.showMessageDialog(null,"phonenumber must be digits and the length should be 10");	
						
					}
					
					else if(!emailValidator.validate(textField_2.getText().trim())) {
						JOptionPane.showMessageDialog(null, "invalid email id: the email id should be like ronald@gmail.com" );   
					}
					else if(isNumber(phNumber)== false)
					{
						JOptionPane.showMessageDialog(null, "invalid Phone Number" );   
						
					}
					else {
						
						try { 
					
							PreparedStatement st = conn.prepareStatement("Insert into agent_details(UserName,Password,Email,PhoneNumber)values(?,?,?,?)");
							System.out.println(st);
							st.setString(1, textField.getText());
							st.setString(2, passwordField.getText());
							st.setString(3, textField_2.getText());
							st.setString(4, textField_3.getText());
							st.executeUpdate();
							JOptionPane.showMessageDialog(null, "registered Successfully");
							st.close();
							conn.close();
							//dispose();
						}
						catch(Exception e1){ 
							System.out.println(e1.getMessage()); 
							} 
			}
					
					
					textField.setText("");
					passwordField.setText("");
					textField_2.setText("");
					textField_3.setText("");
		}
	});
		
		
		btnRegister.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnRegister.setBounds(50, 216, 89, 23);
		contentPane.add(btnRegister);
		
		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Agent_Login_Page al = new Agent_Login_Page();
			al.setVisible(true);
			dispose();
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(299, 216, 89, 23);
		contentPane.add(btnCancel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(171, 72, 136, 20);
		contentPane.add(passwordField);	
	}
}
