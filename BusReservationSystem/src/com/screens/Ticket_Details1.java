package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JToggleButton;

public class Ticket_Details1 extends JFrame {
	java.util.Date date = new java.util.Date();
    long t = date.getTime();
    java.sql.Date sqlDate = new java.sql.Date(t);
	private JPanel contentPane;
	private JTextField tfPassName;
	private JTextField tfNoOfSeats;
	JComboBox<String> sourcecomboBox = new JComboBox();
	JComboBox<String> destinationcomboBox = new JComboBox();
	JComboBox<String> travelNamecomboBox = new JComboBox();
	Connection conn = Jdbc_Connection.conn();
	Integer total = 0;
	Integer totalFare = 0;
	JToggleButton toggleButton = new JToggleButton("1");
	JToggleButton toggleButton_1 = new JToggleButton("2");
	JToggleButton toggleButton_2 = new JToggleButton("3");
	JToggleButton toggleButton_3 = new JToggleButton("4");
	JToggleButton toggleButton_4 = new JToggleButton("5");
	JToggleButton toggleButton_5 = new JToggleButton("6");
	JToggleButton toggleButton_6 = new JToggleButton("7");
	JToggleButton toggleButton_7 = new JToggleButton("8");
	JToggleButton toggleButton_8 = new JToggleButton("9");
	JToggleButton toggleButton_9= new JToggleButton("10");
	JToggleButton toggleButton_10= new JToggleButton("11");
	JToggleButton toggleButton_11= new JToggleButton("12");
	JToggleButton toggleButton_12= new JToggleButton("13");
	JToggleButton toggleButton_13= new JToggleButton("14");
	JToggleButton toggleButton_14= new JToggleButton("15");
	JToggleButton toggleButton_15= new JToggleButton("16");
	JToggleButton toggleButton_16= new JToggleButton("17");
	JToggleButton toggleButton_17= new JToggleButton("18");
	JToggleButton toggleButton_18= new JToggleButton("19");
	JToggleButton toggleButton_19= new JToggleButton("20");
	JToggleButton toggleButton_20= new JToggleButton("21");
	JToggleButton toggleButton_21= new JToggleButton("22");
	JToggleButton toggleButton_22= new JToggleButton("23");
	JToggleButton toggleButton_23= new JToggleButton("24");
	JToggleButton toggleButton_24= new JToggleButton("25");
	JToggleButton toggleButton_25= new JToggleButton("26");
	JToggleButton toggleButton_26= new JToggleButton("27");
	JToggleButton toggleButton_27= new JToggleButton("28");
	JToggleButton toggleButton_28= new JToggleButton("29");
	JToggleButton toggleButton_29= new JToggleButton("30");
	JToggleButton toggleButton_30 = new JToggleButton("31");
	JToggleButton toggleButton_31= new JToggleButton("32");
	JToggleButton toggleButton_32= new JToggleButton("33");
	JToggleButton toggleButton_33= new JToggleButton("34");
	JToggleButton toggleButton_34= new JToggleButton("35");
	JToggleButton toggleButton_35= new JToggleButton("36");
	JToggleButton toggleButton_36 = new JToggleButton("37");
	JToggleButton toggleButton_37 = new JToggleButton("38");
	JToggleButton toggleButton_38 = new JToggleButton("39");
	JToggleButton toggleButton_39 = new JToggleButton("40");
	
	Integer staticFare = 0;
	private JTextField tfFare;
	private final JLabel lblDate = new JLabel(sqlDate.toString());
	private final JLabel lblCurrentdate = new JLabel("CurrentDate:");
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ticket_Details1 frame = new Ticket_Details1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public Ticket_Details1() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Ticket Details1");
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPassengername = new JLabel("PassengerName:");
		lblPassengername.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblPassengername.setBounds(61, 37, 96, 14);
		contentPane.add(lblPassengername);
		
		JLabel lblSource = new JLabel("Source:");
		lblSource.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblSource.setBounds(61, 68, 96, 14);
		contentPane.add(lblSource);
		
		JLabel lblDestination = new JLabel("Destination:");
		lblDestination.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblDestination.setBounds(61, 98, 96, 14);
		contentPane.add(lblDestination);
		
		JLabel lblNoofseats = new JLabel("No Of Seats:");
		lblNoofseats.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNoofseats.setBounds(61, 157, 96, 14);
		contentPane.add(lblNoofseats);
		
		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.setEnabled(false);
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String passName = tfPassName.getText();
				String source = sourcecomboBox.getSelectedItem().toString();
				String dest = destinationcomboBox.getSelectedItem().toString();
				String tname = travelNamecomboBox.getSelectedItem().toString();
				Integer seatnos = Integer.parseInt(tfNoOfSeats.getText());
				Integer fares = Integer.parseInt(tfFare.getText());
				Integer busID = null;
				Integer ticketID = null;
				
				if (passName.length() == 0) {
					JOptionPane.showMessageDialog(null,"Please enter Passenger Name");
				} else {
					/// Getting Bus id
					try {
						String getBusQry = "select bus_id from bus_details where route_id = (\r\n" + 
								"select bus_route.route_id from bus_route where source = ? and Destination = ?) and travels_name = ?";
						PreparedStatement prpST;
						prpST = conn.prepareStatement(getBusQry);
						prpST.setString(1, source);
						prpST.setString(2, dest);
						prpST.setString(3, tname);
						ResultSet rstBusid=prpST.executeQuery();
						while(rstBusid.next())
					    {
							String str = rstBusid.getString(1);
							busID = Integer.parseInt(str);
					    }
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// Inserting into Ticket Details
					try {
						String getTicQry = "INSERT INTO ticket_details (ticket_id,no_of_seats,passenger_name,bus_id,currentdate,FARES) VALUES (id_seq.NEXTVAL,?,?,?,?,?)";
						PreparedStatement prpST;
						prpST = conn.prepareStatement(getTicQry);
						prpST.setString(1, seatnos.toString());
						prpST.setString(2, passName);
						prpST.setString(3, busID.toString());
						prpST.setDate(4, sqlDate);
						prpST.setString(5, fares.toString());
						prpST.executeQuery();
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// getting Ticket id 
					try {
						String TickIDQry = "SELECT * FROM (select ticket_id from ticket_details order by ticket_id desc ) WHERE rownum = 1";
						Statement st1 = conn.createStatement();
						ResultSet rset1=st1.executeQuery(TickIDQry);
						while(rset1.next())
					    {
							ticketID = rset1.getInt(1);
					    }
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// inserting into seat details
					try {
						String getSeatQry = "INSERT INTO seat_details (seat_id,bus_id,ticket_id,seat_no) values (id_seq.NEXTVAL,?,?,?)";
						PreparedStatement st = conn.prepareStatement(getSeatQry);
						if(toggleButton.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 1);
							st.executeUpdate();
						}
						if(toggleButton_1.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 2);
							st.executeUpdate();
						}
						if(toggleButton_2.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 3);
							st.executeUpdate();
						}
						if(toggleButton_3.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 4);
							st.executeUpdate();
						}
						if(toggleButton_4.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 5);
							st.executeUpdate();
						}
						if(toggleButton_5.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 6);
							st.executeUpdate();
						}
						if(toggleButton_6.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 7);
							st.executeUpdate();
						}
						if(toggleButton_7.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 8);
							st.executeUpdate();
						}
						if(toggleButton_8.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 9);
							st.executeUpdate();
						}
						if(toggleButton_9.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 10);
							st.executeUpdate();
						}
						if(toggleButton_10.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 11);
							st.executeUpdate();
						}
						if(toggleButton_11.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 12);
							st.executeUpdate();
						}
						if(toggleButton_12.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 13);
							st.executeUpdate();
						}
						if(toggleButton_13.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 14);
							st.executeUpdate();
						}
						if(toggleButton_14.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 15);
							st.executeUpdate();
						}
						if(toggleButton_15.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 16);
							st.executeUpdate();
						}
						if(toggleButton_16.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 17);
							st.executeUpdate();
						}
						if(toggleButton_17.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 18);
							st.executeUpdate();
						}
						if(toggleButton_18.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 19);
							st.executeUpdate();
						}
						if(toggleButton_19.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 20);
							st.executeUpdate();
						}
						if(toggleButton_20.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 21);
							st.executeUpdate();
						}
						if(toggleButton_21.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 22);
							st.executeUpdate();
						}
						if(toggleButton_22.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 23);
							st.executeUpdate();
						}
						if(toggleButton_23.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 24);
							st.executeUpdate();
						}
						if(toggleButton_24.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 25);
							st.executeUpdate();
						}
						if(toggleButton_25.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 26);
							st.executeUpdate();
						}
						if(toggleButton_26.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 27);
							st.executeUpdate();
						}
						if(toggleButton_27.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 28);
							st.executeUpdate();
						}
						if(toggleButton_28.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 29);
							st.executeUpdate();
						}
						if(toggleButton_29.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 30);
							st.executeUpdate();
						}
						if(toggleButton_30.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 31);
							st.executeUpdate();
						}
						if(toggleButton_31.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 32);
							st.executeUpdate();
						}
						if(toggleButton_32.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 33);
							st.executeUpdate();
						}
						if(toggleButton_33.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 34);
							st.executeUpdate();
						}
						if(toggleButton_34.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 35);
							st.executeUpdate();
						}
						if(toggleButton_35.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 36);
							st.executeUpdate();
						}
						if(toggleButton_36.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 37);
							st.executeUpdate();
						}
						if(toggleButton_37.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 38);
							st.executeUpdate();
						}
						if(toggleButton_38.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 39);
							st.executeUpdate();
						}
						
						if(toggleButton_39.getSelectedObjects() != null) {
							st.setInt(1, busID);
							st.setInt(2, ticketID);
							st.setLong(3, 40);
							st.executeUpdate();
						}			
						// Print Process
						JFrame frame = new JFrame();
						String[] options = new String[2];
						options[0] = new String("Print");
						options[1] = new String("Cancel");
						int res = JOptionPane.showOptionDialog(frame,"Ticket Booking is done!","", 0,JOptionPane.INFORMATION_MESSAGE,null,options,null);
						if (res == JOptionPane.YES_OPTION) {
							Print bd = new Print();
							bd.setVisible(true);
							dispose();
						}  else if (res == JOptionPane.NO_OPTION) {
						      System.out.println("JOptionPane closed");
						}
						// Refresh the Page
						
						tfPassName.setText("");
						btnConfirm.setEnabled(false);
						travelNamecomboBox.setEditable(false);
						travelNamecomboBox.removeAllItems();
						sourcecomboBox.setSelectedItem("Selectsource");
						destinationcomboBox.setSelectedItem("Selectdestination");
						total = 0;
						tfNoOfSeats.setText((total).toString());
						totalFare = 0;
						staticFare = 0;
						tfFare.setText((totalFare).toString());
						toggleButton.setSelected(false);
						toggleButton_1.setSelected(false);toggleButton_2.setSelected(false);toggleButton_3.setSelected(false);toggleButton_4.setSelected(false);toggleButton_5.setSelected(false);
						toggleButton_6.setSelected(false);toggleButton_7.setSelected(false);toggleButton_8.setSelected(false);toggleButton_9.setSelected(false);toggleButton_10.setSelected(false);
						toggleButton_11.setSelected(false);toggleButton_12.setSelected(false);toggleButton_13.setSelected(false);toggleButton_14.setSelected(false);toggleButton_15.setSelected(false);
						toggleButton_16.setSelected(false);toggleButton_17.setSelected(false);toggleButton_18.setSelected(false);toggleButton_19.setSelected(false);toggleButton_20.setSelected(false);
						toggleButton_21.setSelected(false);toggleButton_22.setSelected(false);toggleButton_23.setSelected(false);toggleButton_24.setSelected(false);toggleButton_25.setSelected(false);
						toggleButton_26.setSelected(false);toggleButton_27.setSelected(false);toggleButton_28.setSelected(false);toggleButton_29.setSelected(false);toggleButton_30.setSelected(false);
						toggleButton_31.setSelected(false);toggleButton_32.setSelected(false);toggleButton_33.setSelected(false);toggleButton_34.setSelected(false);toggleButton_35.setSelected(false);
						toggleButton_36.setSelected(false);toggleButton_37.setSelected(false);toggleButton_38.setSelected(false);toggleButton_39.setSelected(false);
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		});
		btnConfirm.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnConfirm.setBounds(92, 527, 89, 23);
		contentPane.add(btnConfirm);
		
		tfPassName = new JTextField();
		tfPassName.setBounds(182, 34, 149, 20);
		contentPane.add(tfPassName);
		tfPassName.setColumns(10);
		
		String query1 ="SELECT source from bus_route";
		Statement st1 = conn.createStatement();
		ResultSet rset1=st1.executeQuery(query1);
		sourcecomboBox.addItem("Selectsource");
		while(rset1.next())
	    {
			sourcecomboBox.addItem(rset1.getString(1));
	    }
		sourcecomboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnConfirm.setEnabled(false);
				travelNamecomboBox.setEditable(false);
				travelNamecomboBox.removeAllItems();
				destinationcomboBox.setSelectedItem("Selectdestination");
				total = 0;
				tfNoOfSeats.setText((total).toString());
				totalFare = 0;
				staticFare = 0;
				tfFare.setText((totalFare).toString());
				toggleButton.setSelected(false);
				toggleButton_1.setSelected(false);toggleButton_2.setSelected(false);toggleButton_3.setSelected(false);toggleButton_4.setSelected(false);toggleButton_5.setSelected(false);
				toggleButton_6.setSelected(false);toggleButton_7.setSelected(false);toggleButton_8.setSelected(false);toggleButton_9.setSelected(false);toggleButton_10.setSelected(false);
				toggleButton_11.setSelected(false);toggleButton_12.setSelected(false);toggleButton_13.setSelected(false);toggleButton_14.setSelected(false);toggleButton_15.setSelected(false);
				toggleButton_16.setSelected(false);toggleButton_17.setSelected(false);toggleButton_18.setSelected(false);toggleButton_19.setSelected(false);toggleButton_20.setSelected(false);
				toggleButton_21.setSelected(false);toggleButton_22.setSelected(false);toggleButton_23.setSelected(false);toggleButton_24.setSelected(false);toggleButton_25.setSelected(false);
				toggleButton_26.setSelected(false);toggleButton_27.setSelected(false);toggleButton_28.setSelected(false);toggleButton_29.setSelected(false);toggleButton_30.setSelected(false);
				toggleButton_31.setSelected(false);toggleButton_32.setSelected(false);toggleButton_33.setSelected(false);toggleButton_34.setSelected(false);toggleButton_35.setSelected(false);
				toggleButton_36.setSelected(false);toggleButton_37.setSelected(false);toggleButton_38.setSelected(false);toggleButton_39.setSelected(false);

			}
		});
		sourcecomboBox.setBounds(182, 65, 149, 20);
		contentPane.add(sourcecomboBox);
		
		String query2 ="SELECT destination from bus_route";
		Statement st2 = conn.createStatement();
		ResultSet rset2=st2.executeQuery(query2);
		destinationcomboBox.addItem("Selectdestination");
		while(rset2.next())
	    {
			destinationcomboBox.addItem(rset2.getString(1));
	    }
		destinationcomboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				total = 0;
				tfNoOfSeats.setText((total).toString());
				totalFare = 0;
				staticFare=0;
				tfFare.setText((totalFare).toString());
				toggleButton.setSelected(false);
				toggleButton_1.setSelected(false);toggleButton_2.setSelected(false);toggleButton_3.setSelected(false);toggleButton_4.setSelected(false);toggleButton_5.setSelected(false);
				toggleButton_6.setSelected(false);toggleButton_7.setSelected(false);toggleButton_8.setSelected(false);toggleButton_9.setSelected(false);toggleButton_10.setSelected(false);
				toggleButton_11.setSelected(false);toggleButton_12.setSelected(false);toggleButton_13.setSelected(false);toggleButton_14.setSelected(false);toggleButton_15.setSelected(false);
				toggleButton_16.setSelected(false);toggleButton_17.setSelected(false);toggleButton_18.setSelected(false);toggleButton_19.setSelected(false);toggleButton_20.setSelected(false);
				toggleButton_21.setSelected(false);toggleButton_22.setSelected(false);toggleButton_23.setSelected(false);toggleButton_24.setSelected(false);toggleButton_25.setSelected(false);
				toggleButton_26.setSelected(false);toggleButton_27.setSelected(false);toggleButton_28.setSelected(false);toggleButton_29.setSelected(false);toggleButton_30.setSelected(false);
				toggleButton_31.setSelected(false);toggleButton_32.setSelected(false);toggleButton_33.setSelected(false);toggleButton_34.setSelected(false);toggleButton_35.setSelected(false);
				toggleButton_36.setSelected(false);toggleButton_37.setSelected(false);toggleButton_38.setSelected(false);toggleButton_39.setSelected(false);
				
				// Travel Name
				
				try {
					String queryTN ="select travels_name, bus_fare from bus_details where route_id = (\r\n" + 
							"select bus_route.route_id from bus_route where source = ? and Destination = ?)";
					PreparedStatement stTN;
					stTN = conn.prepareStatement(queryTN);
					stTN.setString(1, sourcecomboBox.getSelectedItem().toString());
					stTN.setString(2, destinationcomboBox.getSelectedItem().toString());
					ResultSet rsetTN=stTN.executeQuery();
					travelNamecomboBox.removeAllItems();
					travelNamecomboBox.addItem("SelectTravelName");
					while(rsetTN.next())
				    {
						
						travelNamecomboBox.addItem(rsetTN.getString(1));
				    }
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		destinationcomboBox.setBounds(182, 95, 149, 20);
		contentPane.add(destinationcomboBox);
		
		// Travel Name
		travelNamecomboBox.setEditable(false);
				travelNamecomboBox.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						total = 0;
						tfNoOfSeats.setText((total).toString());
						totalFare = 0;
						if ((travelNamecomboBox.getSelectedItem() != "SelectTravelName") || travelNamecomboBox.getSelectedItem() != (null)) {
							System.out.println(travelNamecomboBox.getSelectedItem());
						try {
							
								String query2 ="SELECT bus_fare from bus_details where travels_name = ?";
								PreparedStatement stTN;
								stTN = conn.prepareStatement(query2);
								stTN.setString(1, (String) travelNamecomboBox.getSelectedItem());
								ResultSet rsetTN=stTN.executeQuery();
								while(rsetTN.next())
							    {
									
									
									staticFare = Integer.parseInt(rsetTN.getString(1));
//									tfFare.setText((rsetTN.getString(1)));
									btnConfirm.setEnabled(true);
									
									
									// Fetch the toggle button
									//Default Enabled(true)
									toggleButton.setEnabled(true);
									toggleButton_1.setEnabled(true);toggleButton_2.setEnabled(true);toggleButton_3.setEnabled(true);toggleButton_4.setEnabled(true);toggleButton_5.setEnabled(true);
									toggleButton_6.setEnabled(true);toggleButton_7.setEnabled(true);toggleButton_8.setEnabled(true);toggleButton_9.setEnabled(true);toggleButton_10.setEnabled(true);
									toggleButton_11.setEnabled(true);toggleButton_12.setEnabled(true);toggleButton_13.setEnabled(true);toggleButton_14.setEnabled(true);toggleButton_15.setEnabled(true);
									toggleButton_16.setEnabled(true);toggleButton_17.setEnabled(true);toggleButton_18.setEnabled(true);toggleButton_19.setEnabled(true);toggleButton_20.setEnabled(true);
									toggleButton_21.setEnabled(true);toggleButton_22.setEnabled(true);toggleButton_23.setEnabled(true);toggleButton_24.setEnabled(true);toggleButton_25.setEnabled(true);
									toggleButton_26.setEnabled(true);toggleButton_27.setEnabled(true);toggleButton_28.setEnabled(true);toggleButton_29.setEnabled(true);toggleButton_30.setEnabled(true);
									toggleButton_31.setEnabled(true);toggleButton_32.setEnabled(true);toggleButton_33.setEnabled(true);toggleButton_34.setEnabled(true);toggleButton_35.setEnabled(true);
									toggleButton_36.setEnabled(true);toggleButton_37.setEnabled(true);toggleButton_38.setEnabled(true);toggleButton_39.setEnabled(true);
									
									String qry = "SELECT SEAT_NO from seat_details where bus_id = (select bus_id from bus_details where route_id = (\r\n" + 
											"select bus_route.route_id from bus_route where source = '"+sourcecomboBox.getSelectedItem().toString()+"' and Destination = '"+destinationcomboBox.getSelectedItem().toString()+"') and travels_name = '"+travelNamecomboBox.getSelectedItem().toString()+"')";
									Statement st1 = conn.createStatement();
									ResultSet rset=st1.executeQuery(qry);
									while(rset.next())
								    {
//										JOptionPane.showMessageDialog(null, rset1.getString(1));
										if (rset.getLong(1) == 1) {toggleButton.setEnabled(false);}
										if (rset.getLong(1) == 2) {toggleButton_1.setEnabled(false);}
										if (rset.getLong(1) == 3) {toggleButton_2.setEnabled(false);}
										if (rset.getLong(1) == 4) {toggleButton_3.setEnabled(false);}
										if (rset.getLong(1) == 5) {toggleButton_4.setEnabled(false);}
										if (rset.getLong(1) == 6) {toggleButton_5.setEnabled(false);}
										if (rset.getLong(1) == 7) {toggleButton_6.setEnabled(false);}
										if (rset.getLong(1) == 8) {toggleButton_7.setEnabled(false);}
										if (rset.getLong(1) == 9) {toggleButton_8.setEnabled(false);}
										if (rset.getLong(1) == 10) {toggleButton_9.setEnabled(false);}
										if (rset.getLong(1) == 11) {toggleButton_10.setEnabled(false);}
										if (rset.getLong(1) == 12) {toggleButton_11.setEnabled(false);}
										if (rset.getLong(1) ==13) {toggleButton_12.setEnabled(false);}
										if (rset.getLong(1) == 14) {toggleButton_13.setEnabled(false);}
										if (rset.getLong(1) == 15) {toggleButton_14.setEnabled(false);}
										if (rset.getLong(1) == 16) {toggleButton_15.setEnabled(false);}
										if (rset.getLong(1) == 17) {toggleButton_16.setEnabled(false);}
										if (rset.getLong(1) == 18) {toggleButton_17.setEnabled(false);}
										if (rset.getLong(1) == 19) {toggleButton_18.setEnabled(false);}
										if (rset.getLong(1) == 20) {toggleButton_19.setEnabled(false);}
										if (rset.getLong(1) == 21) {toggleButton_20.setEnabled(false);}
										if (rset.getLong(1) == 22) {toggleButton_21.setEnabled(false);}
										if (rset.getLong(1) == 23) {toggleButton_22.setEnabled(false);}
										if (rset.getLong(1) == 24) {toggleButton_23.setEnabled(false);}
										if (rset.getLong(1) == 25) {toggleButton_24.setEnabled(false);}
										if (rset.getLong(1) == 26) {toggleButton_25.setEnabled(false);}
										if (rset.getLong(1) == 27) {toggleButton_26.setEnabled(false);}
										if (rset.getLong(1) == 28) {toggleButton_27.setEnabled(false);}
										if (rset.getLong(1) == 29) {toggleButton_28.setEnabled(false);}
										if (rset.getLong(1) == 30) {toggleButton_29.setEnabled(false);}
										if (rset.getLong(1) == 31) {toggleButton_30.setEnabled(false);}
										if (rset.getLong(1) == 32) {toggleButton_31.setEnabled(false);}
										if (rset.getLong(1) == 33) {toggleButton_32.setEnabled(false);}
										if (rset.getLong(1) == 34) {toggleButton_33.setEnabled(false);}
										if (rset.getLong(1) == 35) {toggleButton_34.setEnabled(false);}
										if (rset.getLong(1) == 36) {toggleButton_35.setEnabled(false);}
										if (rset.getLong(1) == 37) {toggleButton_36.setEnabled(false);}
										if (rset.getLong(1) == 38) {toggleButton_37.setEnabled(false);}
										if (rset.getLong(1) == 39) {toggleButton_38.setEnabled(false);}
										if (rset.getLong(1) == 40) {toggleButton_39.setEnabled(false);}
								    }
							    }
								
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						}
						
						
						toggleButton.setSelected(false);
						toggleButton_1.setSelected(false);toggleButton_2.setSelected(false);toggleButton_3.setSelected(false);toggleButton_4.setSelected(false);toggleButton_5.setSelected(false);
						toggleButton_6.setSelected(false);toggleButton_7.setSelected(false);toggleButton_8.setSelected(false);toggleButton_9.setSelected(false);toggleButton_10.setSelected(false);
						toggleButton_11.setSelected(false);toggleButton_12.setSelected(false);toggleButton_13.setSelected(false);toggleButton_14.setSelected(false);toggleButton_15.setSelected(false);
						toggleButton_16.setSelected(false);toggleButton_17.setSelected(false);toggleButton_18.setSelected(false);toggleButton_19.setSelected(false);toggleButton_20.setSelected(false);
						toggleButton_21.setSelected(false);toggleButton_22.setSelected(false);toggleButton_23.setSelected(false);toggleButton_24.setSelected(false);toggleButton_25.setSelected(false);
						toggleButton_26.setSelected(false);toggleButton_27.setSelected(false);toggleButton_28.setSelected(false);toggleButton_29.setSelected(false);toggleButton_30.setSelected(false);
						toggleButton_31.setSelected(false);toggleButton_32.setSelected(false);toggleButton_33.setSelected(false);toggleButton_34.setSelected(false);toggleButton_35.setSelected(false);
						toggleButton_36.setSelected(false);toggleButton_37.setSelected(false);toggleButton_38.setSelected(false);toggleButton_39.setSelected(false);
					}
				});
				travelNamecomboBox.setBounds(183, 123, 149, 20);
				contentPane.add(travelNamecomboBox);
		//No of Seats
		tfNoOfSeats = new JTextField();
		tfNoOfSeats.setEditable(false);
		tfNoOfSeats.setBounds(182, 154, 149, 20);
		contentPane.add(tfNoOfSeats);
		tfNoOfSeats.setColumns(10);
		
		JLabel lblSeatNo = new JLabel("Seat No");
		lblSeatNo.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblSeatNo.setBounds(188, 219, 92, 25);
		contentPane.add(lblSeatNo);
		//-----------------------------------------------------------
		// Seat no 
		
		// 1 Seat
		
		toggleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println(toggleButton.isSelected());
				 if (toggleButton.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton.setBounds(43, 255, 51, 23);
		contentPane.add(toggleButton);
		
		// 2 Seat
		
		toggleButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_1.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_1.setBounds(104, 255, 51, 23);
		contentPane.add(toggleButton_1);

		toggleButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_2.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_2.setBounds(165, 255, 51, 23);
		contentPane.add(toggleButton_2);
		
		
		toggleButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_3.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_3.setBounds(250, 255, 51, 23);
		contentPane.add(toggleButton_3);
		

		toggleButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_4.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_4.setBounds(311, 255, 51, 23);
		contentPane.add(toggleButton_4);
		
		
		toggleButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_5.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_5.setBounds(43, 284, 51, 23);
		contentPane.add(toggleButton_5);
		
		
		toggleButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_6.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_6.setBounds(104, 284, 51, 23);
		contentPane.add(toggleButton_6);
		
	
		toggleButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_7.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_7.setBounds(165, 284, 51, 23);
		contentPane.add(toggleButton_7);
		

		toggleButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_8.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_8.setBounds(250, 284, 51, 23);
		contentPane.add(toggleButton_8);
		
		
		toggleButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_9.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_9.setBounds(311, 284, 51, 23);
		contentPane.add(toggleButton_9);
		
	
		toggleButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_10.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_10.setBounds(43, 314, 51, 23);
		contentPane.add(toggleButton_10);
		
		
		toggleButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_11.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_11.setBounds(104, 314, 51, 23);
		contentPane.add(toggleButton_11);
		
		
		toggleButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_12.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_12.setBounds(165, 314, 51, 23);
		contentPane.add(toggleButton_12);
		
	
		toggleButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_13.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_13.setBounds(250, 314, 51, 23);
		contentPane.add(toggleButton_13);
		
	
		toggleButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_14.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_14.setBounds(311, 314, 51, 23);
		contentPane.add(toggleButton_14);
		

		toggleButton_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_15.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_15.setBounds(43, 343, 51, 23);
		contentPane.add(toggleButton_15);
		
		
		toggleButton_16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_16.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_16.setBounds(104, 343, 51, 23);
		contentPane.add(toggleButton_16);
		
		
		toggleButton_17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_17.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_17.setBounds(165, 343, 51, 23);
		contentPane.add(toggleButton_17);
		
		
		toggleButton_18.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_18.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_18.setBounds(250, 343, 51, 23);
		contentPane.add(toggleButton_18);
		
		
		toggleButton_19.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_19.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_19.setBounds(311, 343, 51, 23);
		contentPane.add(toggleButton_19);
		
		
		toggleButton_20.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_20.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_20.setBounds(43, 377, 51, 23);
		contentPane.add(toggleButton_20);
		
		
		toggleButton_21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_21.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_21.setBounds(104, 377, 51, 23);
		contentPane.add(toggleButton_21);
		
		
		toggleButton_22.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_22.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
					 
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_22.setBounds(165, 377, 51, 23);
		contentPane.add(toggleButton_22);
		
		
		toggleButton_23.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_23.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_23.setBounds(250, 377, 51, 23);
		contentPane.add(toggleButton_23);
		
	
		toggleButton_24.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_24.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_24.setBounds(311, 377, 51, 23);
		contentPane.add(toggleButton_24);
		
	
		toggleButton_25.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_25.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_25.setBounds(43, 411, 51, 23);
		contentPane.add(toggleButton_25);
		
		
		toggleButton_26.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_26.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_26.setBounds(104, 411, 51, 23);
		contentPane.add(toggleButton_26);
		
		
		toggleButton_27.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_27.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_27.setBounds(165, 411, 51, 23);
		contentPane.add(toggleButton_27);
		
		
		toggleButton_28.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_28.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_28.setBounds(250, 411, 51, 23);
		contentPane.add(toggleButton_28);
		
		
		toggleButton_29.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_29.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_29.setBounds(311, 411, 51, 23);
		contentPane.add(toggleButton_29);
		
	
		toggleButton_30.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_30.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_30.setBounds(43, 445, 51, 23);
		contentPane.add(toggleButton_30);
		
		
		toggleButton_31.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_31.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_31.setBounds(104, 445, 51, 23);
		contentPane.add(toggleButton_31);
		
		
		toggleButton_32.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_32.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_32.setBounds(165, 445, 51, 23);
		contentPane.add(toggleButton_32);
		
		
		toggleButton_33.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_33.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_33.setBounds(250, 445, 51, 23);
		contentPane.add(toggleButton_33);
		
		
		toggleButton_34.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_34.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_34.setBounds(311, 445, 51, 23);
		contentPane.add(toggleButton_34);
		
		
		toggleButton_35.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_35.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_35.setBounds(43, 481, 51, 23);
		contentPane.add(toggleButton_35);
		
		
		toggleButton_36.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_36.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_36.setBounds(104, 481, 51, 23);
		contentPane.add(toggleButton_36);
		
		
		toggleButton_37.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_37.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_37.setBounds(165, 481, 51, 23);
		contentPane.add(toggleButton_37);
		
		
		toggleButton_38.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_38.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
			}
		});
		toggleButton_38.setBounds(250, 481, 51, 23);
		contentPane.add(toggleButton_38);
		
		
		toggleButton_39.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 if (toggleButton_39.isSelected()) {
					 total = total + 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare + staticFare;
					 tfFare.setText((totalFare).toString());
				 } else {
					 total = total - 1;
					 tfNoOfSeats.setText((total).toString());
					 totalFare = totalFare - staticFare;
					 tfFare.setText((totalFare).toString());
				 }
					
			}
		});
		toggleButton_39.setBounds(311, 481, 51, 23);
		contentPane.add(toggleButton_39);
		//------------------------------------------------
		
		
		
		// Cancel button
		JButton btnCancel = new JButton("LogOut");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Agent_Login_Page bd = new Agent_Login_Page();
			bd.setVisible(true);
			dispose();
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(236, 527, 89, 23);
		contentPane.add(btnCancel);
		
		
		
		JLabel lblTravelName = new JLabel("Travel Name");
		lblTravelName.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblTravelName.setBounds(61, 126, 97, 14);
		contentPane.add(lblTravelName);
		
		JLabel lblFare = new JLabel("Fare");
		lblFare.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblFare.setBounds(61, 188, 46, 14);
		contentPane.add(lblFare);
		
		tfFare = new JTextField();
		tfFare.setEditable(false);
		tfFare.setBounds(182, 185, 149, 20);
		contentPane.add(tfFare);
		tfFare.setColumns(10);
		lblDate.setBounds(182, 12, 97, 14);
		contentPane.add(lblDate);
		lblCurrentdate.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblCurrentdate.setBounds(61, 12, 94, 14);
		
		contentPane.add(lblCurrentdate);
		
		

	}
}
