package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class Add_Bus_Details extends JFrame {

	private JPanel contentPane;
	private JTextField tfTravelName;
	private JTextField textField_1;
	private JTextField tfFare;
	private JTextField tfDepart;
	private JTextField tfArrival;
	private JTextField textField_5;
	JComboBox<String> RoutecomboBox = new JComboBox();
	JComboBox<String> BusTypecomboBox = new JComboBox();
	Connection conn = Jdbc_Connection.conn();
	private JTextField tfBusNo;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Add_Bus_Details frame = new Add_Bus_Details();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public Add_Bus_Details() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle(" Adding BusDetails");
		setBounds(100, 100, 514, 368);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTravelname = new JLabel("Travel Name:");
		lblTravelname.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblTravelname.setBounds(25, 67, 79, 14);
		contentPane.add(lblTravelname);
		
		JLabel lblBustype = new JLabel("Bus Type:");
		lblBustype.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblBustype.setBounds(25, 105, 79, 14);
		contentPane.add(lblBustype);
		
		JLabel lblBusFare = new JLabel("Bus Fare:");
		lblBusFare.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblBusFare.setBounds(25, 187, 62, 14);
		contentPane.add(lblBusFare);
		
		JLabel lblDeparturetime = new JLabel("DepartureTime:");
		lblDeparturetime.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblDeparturetime.setBounds(260, 105, 100, 14);
		contentPane.add(lblDeparturetime);
		
		JLabel lblNewLabel = new JLabel("ArrivalTime:");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel.setBounds(260, 146, 100, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblRouteId = new JLabel("Route Id:");
		lblRouteId.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblRouteId.setBounds(25, 146, 79, 14);
		contentPane.add(lblRouteId);
		
		JLabel lblNoOfSeats = new JLabel("No Of Seats:");
		lblNoOfSeats.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNoOfSeats.setBounds(260, 187, 100, 14);
		contentPane.add(lblNoOfSeats);
		
		JButton btnAdd = new JButton("Add");
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String vTravelName = tfTravelName.getText();
				String vBusNo = tfBusNo.getText();
				String vBusType = BusTypecomboBox.getSelectedItem().toString();
				String vFare = tfFare.getText();
				String vDepart = tfDepart.getText();
				String vArrival = tfArrival.getText();
				String vRouteId = RoutecomboBox.getSelectedItem().toString();
				//String vseats = tfseats.getText();
				try {  
					if(vTravelName.length() == 0 || vBusNo.length()== 0 
							|| "SelectBusType".equals(vBusType) 
							|| vFare.length() == 0 || vDepart.length()== 0 || vArrival.length()== 0 
							|| "SelectRouteId".equals(vRouteId) )      //check condition null
					{
						JOptionPane.showMessageDialog(null,"Please enter valid details ");
					} 
					
					else {
						// get Bus Type ID 
						String getBusType = "";
						String query ="SELECT BUS_TYPE_ID from BUS_TYPE where BUS_TYPE_NAME = '"+vBusType+"'";
						Statement st1 = conn.createStatement();
						ResultSet rset = st1.executeQuery(query);
						while(rset.next())
					    {
							getBusType = rset.getString(1);
					    }
						// Add values into  bus_details
						String qry = "Insert into bus_details(bus_id,TRAVELS_NAME,BUS_NUMBER,bus_type_id,bus_fare,departure_time,arrival_time,route_id,no_of_seats)values(id_seq.nextval,?,?,?,?,?,?,?,40)";
						PreparedStatement st = conn.prepareStatement(qry);
						st.setString(1, vTravelName);
						st.setString(2, vBusNo);
						st.setString(3, getBusType);
						st.setString(4, vFare);
						st.setString(5, vDepart);
						st.setString(6, vArrival);
						st.setString(7, vRouteId);
						//st.setString(8, vseats);
						st.execute();
						JOptionPane.showMessageDialog(null, "added Successfully");
						conn.close();
						//dispose();
					}
					
			}
			catch(Exception e1){ 
				System.out.println(e1.getMessage()); 
			} 
				
				tfTravelName.setText("");	
				tfBusNo.setText("");
				BusTypecomboBox.setSelectedItem("selectBysType");
				tfFare.setText("");
				tfDepart.setText("");
				tfArrival.setText("");
				RoutecomboBox.setSelectedItem("SelectRouteId");
				//tfseats.setText("");
				
		}	
	});
		
		btnAdd.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnAdd.setBounds(260, 286, 89, 23);
		contentPane.add(btnAdd);
		
		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Bus_Details bd = new Bus_Details();
			bd.setVisible(true);
			dispose();
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(379, 286, 89, 23);
		contentPane.add(btnCancel);
		
		tfTravelName = new JTextField();
		tfTravelName.setBounds(114, 64, 132, 20);
		contentPane.add(tfTravelName);
		tfTravelName.setColumns(10);
		
//		textField_1 = new JTextField();
//		textField_1.setBounds(133, 51, 132, 20);
//		contentPane.add(textField_1);
//		textField_1.setColumns(10);
		
		String query1 ="SELECT BUS_TYPE_NAME from BUS_TYPE";
		Statement st1 = conn.createStatement();
		ResultSet rset=st1.executeQuery(query1);
		BusTypecomboBox.addItem("SelectBusType");
		while(rset.next())
	    {
//			JOptionPane.showMessageDialog(null, rset1.getString(1));
			BusTypecomboBox.addItem(rset.getString(1));
	    }
		BusTypecomboBox.setBounds(114, 102, 132, 20);
		contentPane.add(BusTypecomboBox);
		
		tfFare = new JTextField();
		tfFare.setBounds(112, 184, 89, 20);
		contentPane.add(tfFare);
		tfFare.setColumns(10);
		
		tfDepart = new JTextField();
		tfDepart.setBounds(370, 102, 89, 20);
		contentPane.add(tfDepart);
		tfDepart.setColumns(10);
		
		tfArrival = new JTextField();
		tfArrival.setBounds(370, 143, 89, 20);
		contentPane.add(tfArrival);
		tfArrival.setColumns(10); 
		
		
		String query ="select route_id  from bus_route";
		Statement st = conn.createStatement();
		rset=st.executeQuery(query);
		RoutecomboBox.addItem("SelectRouteId");
		while(rset.next())
	    {
			RoutecomboBox.addItem(rset.getString(1));
	    }
		RoutecomboBox.setBounds(112, 143, 132, 20);
		contentPane.add(RoutecomboBox);
		
		JLabel lblBu = new JLabel("Bus Number:");
		lblBu.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBu.setBounds(260, 67, 89, 14);
		contentPane.add(lblBu);
		
		tfBusNo = new JTextField();
		tfBusNo.setBounds(370, 64, 89, 20);
		contentPane.add(tfBusNo);
		tfBusNo.setColumns(10);
		
		JLabel label = new JLabel("40");
		label.setFont(new Font("Times New Roman", Font.BOLD, 13));
		label.setBounds(370, 187, 89, 14);
		contentPane.add(label);
	}
}
