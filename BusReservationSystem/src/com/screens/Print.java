package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.dao.Jdbc_Connection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.PrintJob;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Print extends JFrame {
	
	Connection conn = Jdbc_Connection.conn();
	private JPanel contentPane;
	private String vDate = null;
	private String vPassName = null;
	private String vFares = null;
	private String vSeatNo = null;
	private String vTravelName = null;
	private String vBus_Type = null;
	private String vBusNo = null;
	private String vDepTime = null;
	private String vArrTime = null;
	private String vSource = null;
	private String vDest = null;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Print frame = new Print();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public Print() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 320);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 450, 35);
		contentPane.add(panel);
		
		JLabel lblPrintWindow = new JLabel("Print Window");
		lblPrintWindow.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel.add(lblPrintWindow);
		
		JPanel printPanel = new JPanel();
		printPanel.setBounds(10, 57, 450, 162);
		contentPane.add(printPanel);
		printPanel.setLayout(null);
		
		// Fetching content from Database
		String query1 ="\r\n" + 
				"select A.CURRENTDATE,A.PASSENGER_NAME,A.FARES,\r\n" + 
				"(SELECT LISTAGG(SEAT_NO,',') WITHIN GROUP (ORDER BY SEAT_NO) SEAT_NOS\r\n" + 
				"FROM SEAT_DETAILS \r\n" + 
				"WHERE TICKET_ID = (SELECT * FROM (SELECT TICKET_ID FROM TICKET_DETAILS ORDER BY TICKET_ID DESC) WHERE ROWNUM = 1)) AS SEAT_NOS\r\n" + 
				", C.TRAVELS_NAME,E.BUS_TYPE_NAME, C.BUS_NUMBER,C.DEPARTURE_TIME, C.ARRIVAL_TIME,\r\n" + 
				"D.SOURCE, D.DESTINATION\r\n" + 
				"from ticket_details A \r\n" + 
				"INNER JOIN BUS_DETAILS C ON A.BUS_ID = C.BUS_ID\r\n" + 
				"INNER JOIN BUS_ROUTE D ON C.ROUTE_ID = D.ROUTE_ID\r\n" + 
				"INNER JOIN BUS_TYPE E ON E.BUS_TYPE_ID = C.BUS_TYPE_ID\r\n" + 
				"WHERE A.TICKET_ID = (SELECT * FROM (SELECT TICKET_ID FROM TICKET_DETAILS ORDER BY TICKET_ID DESC) WHERE ROWNUM = 1)";
		Statement st1 = conn.createStatement();
		ResultSet rset1=st1.executeQuery(query1);
		while(rset1.next())
	    {
			Date date = rset1.getDate(1);  
		    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		    String strDate= formatter.format(date); 
			vDate = strDate;
			vPassName = rset1.getString(2);
			vFares = rset1.getString(3);
			vSeatNo = rset1.getString(4);
			vTravelName = rset1.getString(5);
			vBus_Type = rset1.getString(6);
			vBusNo = rset1.getString(7);
			vDepTime = rset1.getString(8);
			vArrTime = rset1.getString(9);
			vSource = rset1.getString(10);
			vDest = rset1.getString(11);
	    }
		JLabel lblPassengerName = new JLabel("Passenger Name");
		lblPassengerName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPassengerName.setBounds(10, 35, 94, 14);
		printPanel.add(lblPassengerName);
		
		JLabel lblDate = new JLabel(" Date");
		lblDate.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDate.setBounds(10, 10, 79, 14);
		printPanel.add(lblDate);
		
		JLabel lblTravelsname = new JLabel("Travels Name");
		lblTravelsname.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTravelsname.setBounds(10, 63, 79, 14);
		printPanel.add(lblTravelsname);
		
		JLabel lblNewLabel = new JLabel("Bus Type");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(10, 88, 79, 14);
		printPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Bus No");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1.setBounds(10, 113, 79, 14);
		printPanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Source");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_2.setBounds(246, 10, 69, 14);
		printPanel.add(lblNewLabel_2);
		
		JLabel lblDestination = new JLabel("Destination");
		lblDestination.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDestination.setBounds(246, 35, 69, 14);
		printPanel.add(lblDestination);
		
		JLabel lblNewLabel_3 = new JLabel("Dept Time");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_3.setBounds(246, 63, 69, 14);
		printPanel.add(lblNewLabel_3);
		
		JLabel lblArrTime = new JLabel("Arr Time");
		lblArrTime.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblArrTime.setBounds(246, 88, 69, 14);
		printPanel.add(lblArrTime);
		
		JLabel lblSeatNos = new JLabel("Seat No's");
		lblSeatNos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSeatNos.setBounds(246, 113, 69, 14);
		printPanel.add(lblSeatNos);
		
		JLabel lblFares = new JLabel("Fares");
		lblFares.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFares.setBounds(246, 137, 69, 14);
		printPanel.add(lblFares);
		
		JLabel lblA = new JLabel(vDate);
		lblA.setBounds(125, 10, 111, 14);
		printPanel.add(lblA);
		
		JLabel lblB = new JLabel(vPassName);
		lblB.setBounds(125, 35, 111, 14);
		printPanel.add(lblB);
		
		JLabel lblC = new JLabel(vTravelName);
		lblC.setBounds(125, 63, 111, 14);
		printPanel.add(lblC);
		
		JLabel lblD = new JLabel(vBus_Type);
		lblD.setBounds(125, 88, 111, 14);
		printPanel.add(lblD);
		
		JLabel lblE = new JLabel(vBusNo);
		lblE.setBounds(125, 113, 111, 14);
		printPanel.add(lblE);
		
		JLabel lblW = new JLabel(vSource);
		lblW.setBounds(340, 10, 100, 14);
		printPanel.add(lblW);
		
		JLabel lblR = new JLabel(vDest);
		lblR.setBounds(340, 35, 100, 14);
		printPanel.add(lblR);
		
		JLabel lblTt = new JLabel(vDepTime);
		lblTt.setBounds(340, 63, 100, 14);
		printPanel.add(lblTt);
		
		JLabel lblRt = new JLabel(vArrTime);
		lblRt.setBounds(340, 88, 100, 14);
		printPanel.add(lblRt);
		
		JLabel lblGg = new JLabel(vSeatNo);
		lblGg.setBounds(340, 113, 100, 14);
		printPanel.add(lblGg);
		
		JLabel lblTte = new JLabel(vFares);
		lblTte.setBounds(340, 137, 100, 14);
		printPanel.add(lblTte);
		
		JButton btnPrint = new JButton("Print");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,"printed successfully");	
			//dispose();
			}
		});
		btnPrint.setBounds(202, 238, 89, 23);
		contentPane.add(btnPrint);
	}
}
