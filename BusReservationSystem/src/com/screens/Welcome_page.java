package com.screens;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

public class Welcome_page extends JFrame {
	
	private JPanel contentPane;
	/**
	 * @wbp.nonvisual location=61,9
	 */

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Welcome_page frame1 = new Welcome_page();
					frame1.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
					
	public void close() {
		WindowEvent winClosingEvent=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
	}

	/**
	 * Create the frame.
	 */
	public Welcome_page() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Welcome Page");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWelcomeToBus = new JLabel("Welcome To Bus Reservation System");
		lblWelcomeToBus.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblWelcomeToBus.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcomeToBus.setBounds(48, 93, 349, 64);
		contentPane.add(lblWelcomeToBus);
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();	
			Login_Page lp = new Login_Page();
			lp.setVisible(true);

			}
		});
		btnEnter.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnEnter.setBounds(160, 194, 101, 23);
		contentPane.add(btnEnter);
		
		ImageIcon image = new ImageIcon("E:\\Vengared.PNG");
		JLabel lblNewLabel = new JLabel(image);
		lblNewLabel.setBounds(160, 45, 101, 51);
		contentPane.add(lblNewLabel);
	}
}
